#!/usr/bin/env bash

# install ansible (http://docs.ansible.com/intro_installation.html)
sudo apt-get -y install software-properties-common
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update
sudo apt-get -y install ansible

# copy examples into /home/vagrant (from inside the mgmt node)
cp -a /vagrant/examples/* /home/vagrant
chown -R vagrant:vagrant /home/vagrant

# configure hosts file for our internal network defined by Vagrantfile
cat >> /etc/hosts <<EOL

# vagrant environment nodes
192.168.50.10  mgmt
192.168.50.11  lb
192.168.50.21  web1
192.168.50.22  web2
192.168.50.23  web3
192.168.50.24  web4
192.168.50.25  web5
192.168.50.26  web6
192.168.50.27  web7
192.168.50.28  web8
192.168.50.29  web9
EOL

# ssh-keyscan lb web1 web2 >> ~/.ssh/known_hosts
